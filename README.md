# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* **Worksheets for the following R Packages:**
* **Intervals**: Comparing two interval of numbers (i.e 1-100 and 50-150)
* **Pathview**: Plotting KEGG pathways
* **NMF**: non-negative matrix factorization clustering
* **annotationDbi**: Converting gene names between styles (i.e. HUGO -> EntrezID -> ENSG)
* **biomaRt**: Annotation of small segments using an online database (ONLY GOOD FOR QUICK CHECKS!)
* **DNAcopy**: Implementation of Circular binary segmentation on a random dataset
* **omicCircos**: Circos plots... just a mess
* **getopt**: Taking in labelled arguments rather than indexed (i.e. Rscript test.sh -a first_arg -b second_arg
* **rtsne**: Implementation of t-SNE on MNIST dataset, compare with PCA
* **neuralnets**: Implementation of logistic neural network compared to linear regression

### How do I get set up? ###

* Clone the R package to your local and pull the latest version
* Run the worksheet for the demo you are using

### Contribution guidelines ###

* Please name any contributed R Package worksheets using the following convention:
[R-package]_worksheet.R

* Please fill out and include the following information into your script

```
#!R

# NMF vignette: http://nmf.r-forge.r-project.org/vignettes/NMF-vignette.pdf
# Created by Rene
# Worksheet for package: NMF v0.20.6
# Date: May-25-2016 

```