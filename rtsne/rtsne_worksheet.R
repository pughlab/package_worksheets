library(Rtsne)
library(vegan)
library(tsne)

#MNIST data downloaded from: https://drive.google.com/file/d/0B6E7D59TV2zWYlJLZHdGeUYydlk/view
train<- read.csv("~/git/package_worksheets/rtsne/train.csv") 

set.seed(1)
## Curating the database for analysis with both t-SNE and PCA
Labels<-train$label
train$label<-as.factor(train$label)
## for plotting
colors = rainbow(length(unique(train$label)))
names(colors) = unique(train$label)

## Executing the algorithm on curated data
for(perp.val in seq(2, 100, by=2)){
  set.seed(1)
  message(paste0("Perplexity: ", perp.val))
  tsne <- Rtsne(train[,-1], dims = 2, perplexity=perp.val, verbose=TRUE, max_iter = 500)
  
  ## Plotting
  png(paste0("~/git/package_worksheets/rtsne/png/perp", perp.val, "-tsne.png"),
      width=1500, height=1500, res=80)
  plot(tsne$Y, t='n', main="tsne")
  text(tsne$Y, labels=train$label, col=colors[train$label])
  dev.off()
}




# run principle component analysis
pc<-prcomp(train[,-1])
pdf(paste0("~/git/package_worksheets/rtsne/png/pcaMNIST.pdf"))
screeplot(pc)

for(j in c(2:9)){
  i <- j - 1
  plot(pc$x[,i], pc$x[,j], col=train[,1], pch=16,
       xlab = paste0("PC", i), ylab=paste0("PC", j))
  # plot spiderweb and connect outliners with dotted line
  ordispider(cbind(pc$x[,i], pc$x[,j]), factor(train[,1]),lwd=0.2, label = TRUE)
  ordihull(cbind(pc$x[,i], pc$x[,j]), factor(km$cluster), lty = "dotted")
}
dev.off()
