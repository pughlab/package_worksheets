library(neuralnet)
# Paper: 
set.seed(500)
library(MASS)
data <- Boston

# Check for missing datapoints
apply(data,2,function(x) sum(is.na(x)))

# Split into training and test set, and then run linear regression
index <- sample(1:nrow(data),round(0.75*nrow(data)))
train <- data[index,]
test <- data[-index,]
lm.fit <- glm(medv~., data=train)
summary(lm.fit)
pr.lm <- predict(lm.fit,test)
MSE.lm <- sum((pr.lm - test$medv)^2)/nrow(test)

# Data preprocessing: normalization using min-max to [0,1] before neuralnets
maxs <- apply(data, 2, max) 
mins <- apply(data, 2, min)

scaled <- as.data.frame(scale(data, center = mins, scale = maxs - mins))
train_ <- scaled[index,]
test_ <- scaled[-index,]

## 2 hidden layers with this configuration: 13:5:3:1
# Rule of thumb: hidden layer neurons are usually 2/3 of the input size. 
n <- names(train_)
f <- as.formula(paste("medv ~", paste(n[!n %in% "medv"], collapse = " + ")))
nn <- neuralnet(formula=f, data=train_,
                hidden=c(5,3),
                linear.output=T,  # TRUE: does not apply activation function to output layer
                algorith="rprop+",   # resilient backprop with weight learningrate, 
                err.fct = 'sse',   # SSE: differentiable error function
                act.fct = 'logistic',   #activation function: logistic of tanh
                stepmax=5000,   # max number of steps to converge
                threshold=0.01)  # Error threshold for optimal solution
# Black lines: weights for connections between layers (synapses)
# Blue lines: bias term added
plot(nn)

# Plot of generalized weights
par(mfrow=c(ceiling(sqrt(ncol(data))),
            ceiling(sqrt(ncol(data)))))
par(mar=c(5.1, 4.1, 1, 1))
for(each_covariate in colnames(data)[-grep("medv", colnames(data))]){
  gwplot(nn,selected.covariate=each_covariate, min=-2.5, max=5, las=1)
}
# Gen.weights ~0 == no effect (e.g. age)
# Gen.weights >1 == non-linear effect (e.g. rm)

# Predict based on NN; revert the prediction back to original
pr.nn <- compute(nn,test_[,1:13])

pr.nn_ <- pr.nn$net.result*(max(data$medv)-min(data$medv))+min(data$medv)
test.r <- (test_$medv)*(max(data$medv)-min(data$medv))+min(data$medv)

MSE.nn <- sum((test.r - pr.nn_)^2)/nrow(test_)


## Compute the probability of a covariate combination
sample_train_ <- train_[-grep("medv", colnames(train_))]
sample_test_ <- train_[-grep("medv", colnames(test_))]
from_train_data <- as.matrix(sample_train_[runif(3, 1, nrow(data_)),])
from_test_data <- as.matrix(sample_test_[runif(3, 1, nrow(data_)),])
mean_from_data <- apply(sample_test_, 2, mean)
runif_from_data <- apply(sample_test_, 2, function(x){
  runif(1, min=min(x), max=max(x))
  })
gaussian_from_data <- apply(sample_test_, 2, function(x){
  rnorm(1, mean(x), sd(x))
})
gen_sample_mat <- rbind(from_train_data,
                        from_test_data,
                        mean_from_data,
                        runif_from_data,
                        gaussian_from_data)
new_output <- compute(nn, covariate=gen_sample_mat)
new_output$net.result


## Calculate the uncertainty
# Restrictions: ONLY applicable if covariate has a 
# non-zero effect and if it is linearly independent
# For example: assumes linearly independent and all relevant
nn_new <- neuralnet(formula=f, data=train_,
                    hidden=c(8), linear.output=T)
ci <- confidence.interval(nn_new, alpha=0.05)





######## Understanding output
# net.result - a list containing the overall result,
#   i.e. the output, of the neural network for each 
#   replication
#
# weights - a list containing the fitted weights of
#   the neural network for each replication.
#
# generalized.weights - The generalized weight
#   is defined as the contribution of the ith 
#   covariate to the log-odds.
#     > The generalized weight expresses the effect of each
#   covariate x_i and thus has an analogous interpretation
#   as  the ith  regression  parameter  in  regression
#   models
#
# result.matrix - a  matrix  containing  the  error,
#   reached threshold, needed steps, AIC and BIC
#   (computed if likelihood=TRUE) and estimated
#   weights for each replication. Each column rep-
#   resents one replication.
#
# startweights - a  list  containing  the  starting
#   weights for each replication.

########### Interpreting the summary
head(nn$result.matrix)
# 1
# error                    0.448928697772
# reached.threshold        0.009656885299
# steps                 3684.000000000000
# Intercept.to.1layhid1   -0.179319046315
# crim.to.1layhid1       -59.302019071317
# zn.to.1layhid1           3.571306677573
#
# Steps:  until all partial derivatives of the function
#   are lower than 'threshold'
# Weights:  -0.179 for the first hidden neuron of first hidden layer
#   -59.30, 3.57, ..., weights leading to first hidden neuron

# Covariates
# net.result - fitted values of g(x)
out <- cbind(nn$covariate, nn$net.result[[1]])
dimnames(out) <- list(NULL,
                      c(colnames(data)[-ncol(data)], "nn-output"))

